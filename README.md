# Docker-Compose-Latex

## installation guide

### clone
```
git clone https://gitlab.com/MuellerLionel/docker-compose-latex.git
cd docker-compose-latex
```

### edit .tex file
```
vim main.tex
```

### compile
```
docker-compose -f docker-compose.yml up -d
```

